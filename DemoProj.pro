TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += \
    include

SOURCES += src/main.c
    

HEADERS += \
   include/definitions.h
    
OTHER_FILES += \
    LICENSE \
    README.md 
